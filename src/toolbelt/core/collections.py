from typing import TypeVar, Generic, Any, Iterable, List

StackItem = TypeVar('StackItem', bound = Any)

class Stack(Generic[StackItem]):
  class IsEmpty(ValueError): pass

  def __init__(self, initials: Iterable[StackItem] = None):
    self._container: List[StackItem] = list(initials) if initials else []

  @property
  def size(self) -> int:
    return len(self._container)

  @property
  def is_empty(self) -> bool:
    return self.size == 0

  @property
  def head(self) -> StackItem:
    if self.is_empty: raise  self.IsEmpty
    else:             return self._container[-1]

  def push(self, item: StackItem) -> StackItem:
    self._container.append(item)

    return item

  def pop(self) -> StackItem:
    if self.is_empty: raise  self.IsEmpty
    else:             return self._container.pop()
