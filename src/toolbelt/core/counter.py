import threading

class Counter:
  def __init__(self, *, initial_value: int = 0):
    self._value = initial_value
    self._lock  = threading.Lock()

  @property
  def current_value(self) -> int:
    return self._value

  def fetch_and_add(self) -> int:
    value = self._value

    with self._lock: self._value += 1

    return value
