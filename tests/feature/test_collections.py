import pytest

from toolbelt.core.collections import Stack

class StackTests:
  def test_stack(self):
    stack = Stack[int]()

    assert stack.size     == 0
    assert stack.is_empty is True

    with pytest.raises(Stack.IsEmpty):
      assert stack.pop()

    first_pushed = stack.push(9)

    assert first_pushed   == 9
    assert stack.size     == 1
    assert stack.is_empty is False
    assert stack.head     == first_pushed

    second_pushed = stack.push(18)

    assert second_pushed  == 18
    assert stack.size     == 2
    assert stack.is_empty is False
    assert stack.head     == second_pushed

    first_poped = stack.pop()

    assert first_poped    == second_pushed
    assert stack.size     == 1
    assert stack.is_empty is False
    assert stack.head     == first_pushed

    second_poped = stack.pop()

    assert second_poped   == first_pushed
    assert stack.size     == 0
    assert stack.is_empty is True

    with pytest.raises(Stack.IsEmpty):
      assert stack.head

  def test_stack_initials(self):
    stack = Stack[int]([18, 9])

    assert stack.size == 2
    assert stack.head == 9

    stack.pop()

    assert stack.size == 1
    assert stack.head == 18
