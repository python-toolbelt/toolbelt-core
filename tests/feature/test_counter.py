from toolbelt.core.counter import Counter

def test_counter():
  counter = Counter()

  assert counter.current_value   == 0
  assert counter.fetch_and_add() == 0
  assert counter.current_value   == 1

def test_counter_initial_value():
  counter = Counter(initial_value = 10)

  assert counter.current_value   == 10
  assert counter.fetch_and_add() == 10
  assert counter.current_value   == 11
