# Changelog

Tracking changes in `toolbelt-core` between versions. For a complete view of all the releases, visit GitLab:

https://gitlab.com/python-toolbelt/toolbelt-core/-/releases

## next release
- added naive implementation of `Stack` structure
- added `Counter`
